<?php

// src/Command/ExpensesReindexCommand.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use thiagoalessio\TesseractOCR\TesseractOCR;


class ExpensesReindexCommand extends Command
{
// the name of the command (the part after "bin/console")
  protected static $defaultName = 'app:expenses-reindex';

  protected function configure()
  {
    $this
      // the short description shown while running "php bin/console list"
      ->setDescription('Reindex expenses.')
      // the full command description shown when running the command with
      // the "--help" option
      ->setHelp('Reindex expenses images and pdf files in specific folder.')
      ->addArgument('dir', InputArgument::REQUIRED, 'The directory');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    // outputs multiple lines to the console (adding "\n" at the end of each line)
    $output->writeln([
      'Directory index',
      '============',
      '',
    ]);

    // the value returned by someMethod() can be an iterator (https://secure.php.net/iterator)
    // that generates and returns the messages with the 'yield' PHP keyword
    $output->writeln($this->listFiles($input, $output, $input->getArgument('dir')));

    // $output->write('create a user.');

    $output->writeln('Username: ' . $input->getArgument('dir'));
  }

  protected function listFiles(InputInterface $input, OutputInterface $output, $dir) {
    $finder = new Finder();
    // find all files in the current directory.
    $finder
      ->files()
      ->in($dir);
      // ->in(__DIR__);

    // check if there are any search results.
    if ($finder->hasResults()) {
      // ...
    }

    $results = [];
    $headers = ['date' => 'skip', 'category (refreshments, marketing, travel)' => 'refreshments', 'price' => 'skip', 'GST' => 'skip'];
    foreach ($finder as $file) {
      $output->writeln($file->getRealPath());
      // $absoluteFilePath = $file->getRealPath();
      // $fileNameWithExtension = $file->getRelativePathname();
      // $result[] = $file->getFilename();

      $answer = "";
      if (strpos(strtolower($file->getFilename()), 'jpg') > 0) {
        $output->writeln("===========");
        $ocr = (new TesseractOCR($file->getRealPath()))->run();
        $output->writeln($ocr);
        $output->writeln("===========");
        $output->writeln("CURRENT FILENAME : " . $file->getFilename());
        $output->writeln("===========");
        $result = [];
        foreach ($headers as $header => $default) {
          $helper = $this->getHelper('question');
          if ($header === 'GST') {
            $default = (int) $answer / 11;
            $default = round(($default == 0) ? '0.00' : $default, 2);
          }
          elseif ($header === 'date') {
            if ($pos = strpos($ocr, '2019') > 0) {
              $pos = (($pos - 10) > 0) ? $pos : 0;
              $default = str_replace("\n", '', substr($ocr, $pos - 10, 20));
            }
          }
          $question = new Question("Enter " . $header . " [default : " . $default . "], ['skip' to skip]: ", $default);
          $answer = $helper->ask($input, $output, $question);
          if ($answer == 'skip') {
            $result = NULL;
            break;
          }
          else {
            $result[$header] = $answer;
          }
        }
      }
      if ($result) {
        $resultFilename = implode('-', array_values($result)) . 'GST.jpg';
        $output->writeln("===========NEW NAME");
        $output->writeln($resultFilename);
        $results[$file->getFilename()] = $result;

        // Rename file.
        if ($resultFilename !== $file->getFilename()) {
          $filesystem = new Filesystem();
          $filesystem->rename($file->getRealPath(), str_replace($file->getFilename(), $resultFilename, $file->getRealPath()));
        }
      }

    }

    return $result;
  }

}
