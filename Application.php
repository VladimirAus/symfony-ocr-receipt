#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use App\Command\ExpensesReindexCommand;
use Symfony\Component\Console\Application;

$application = new Application('expenses', '1.0.0');

$application->add(new ExpensesReindexCommand());

//$application->setDefaultCommand($command->getName(), true);
$application->run();

/*$application = new Application('reindex', '1.0.0');

// Register commands.
$application
  ->register('reindex')
  ->addArgument('dir', InputArgument::REQUIRED, 'The directory');
  //->addOption('bar', null, InputOption::VALUE_REQUIRED);

$application->run();*/

/*use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

(new Application('echo', '1.0.0'))
  ->register('echo')
  ->addArgument('foo', InputArgument::OPTIONAL, 'The directory')
  ->addOption('bar', null, InputOption::VALUE_REQUIRED)
  ->setCode(function(InputInterface $input, OutputInterface $output) {
    // Output arguments and options.
    dump($input);
  })
  ->getApplication()
  ->setDefaultCommand('echo', true) // Single command application
  ->run();*/
